function data()
return {
	laneConfig = {
		{ forward = true },
		{ forward = true },
		{ forward = true },
	},
	streetWidth = 3.0,
	sidewalkWidth = 1.5,
	sidewalkHeight = .3,
	yearFrom = 1850,
	yearTo = 0,
	upgrade = false,
	country = true,
	speed = 70.0,
	transportModesStreet = { "BUS", "TRAM" },
	type = "one way country new small",
	name = _("Tram mit Gehweg, Betonschwellen").." (11)",
	desc = _("Oneway Tram, 1-lane, speed limit %2%, Size Nm(1000mm)") .. "\n" .. _("Hold 'Shift' while painting with any brush texture to overpaint the ground texture."),
	categories = { "07_Tram_mit_Gehweg" },
	materials = {	
		streetLane = {
			name = "street/rtp_streetlane_betonschwellen.mtl",
			size = { 6.0, 2.2 }
		},	
		streetTramTrack = {
			name = "street/new_medium_tram_track.mtl",
			size = { 2.0, 2.0 }	
		},
		crossingTramTrack = {
			name = "street/new_medium_tram_track.mtl",
			size = { 2.0, 2.0 }
		},
		crossingLane = {
			name = "street/rtp_streetlane_betonschwellen.mtl",
			size = { 6, 2.2 }
		},
		sidewalkPaving = {
			name = "street/airport/airport_runway_medium_paving.mtl",
			size = { 4.0, 1.5 }
		},
		sidewalkBorderInner = {
			name = "street/new_medium_sidewalk_border_inner.mtl",		
			size = { 3, 0.6 }
		},
		sidewalkBorderOuter = {
			name = "street/new_medium_sidewalk_border_outer.mtl",		
			size = { 8.0, 0.41602 }
		},
		sidewalkCurb = {
			name = "street/new_medium_sidewalk_curb.mtl",
			size = { 3, .35 }
		},
		sidewalkWall = {
			name = "street/new_medium_sidewalk_wall.mtl",
			size = { 8.0, 0.41602 }
		}	
	},
	assets = {
		{
			name = "asset/domtrain_kofferleuchte.mdl",
			offset = 7.5,
			distance = 20.0,
			prob = 1.0,
			offsetOrth = 0.3,
			randRot = false,
			oneSideOnly = true,
			alignToElevation = false,
			avoidFaceEdges = false,
			placeOnBridge = true,
		}, 
	},
	cost = 25.0,
}
end
