function data()
return {
	numLanes = 2,
	streetWidth = 6.0,
	sidewalkWidth = 0.7,
	sidewalkHeight = .0,
	yearFrom = 1850,
	yearTo = 0,
	upgrade = false,
	country = true,
	speed = 70.0,
	transportModesStreet = { "BUS", "TRAM" },
	type = "new medium",
	name = _("Tram ohne Gehweg, Holzschwellen").." (22)",
	desc = _("Tram, 2-lane, speed limit %2%, Size Nm(1000mm)") .. "\n" .. _("Hold 'Shift' while painting with any brush texture to overpaint the ground texture."),
	categories = { "06_Tram_ohne_Gehweg" },
	materials = {
		streetLane = {
			name = "street/rtp_streetlane_holzschwellen.mtl",
			size = { 6.0, 2.2 }
		},	
		streetTramTrack = {
			name = "street/new_medium_tram_track.mtl",
			size = { 2.0, 2.0 }	
		},
		crossingLane = {
			name = "street/rtp_streetlane_holzschwellen.mtl",
			size = { 6, 2.2 }
		},
		crossingTramTrack = {
			name = "street/new_medium_tram_track.mtl",
			size = { 2.0, 2.0 }
		},
	},	
	assets = {
	},
	catenary = {
		pole = {
			name = "asset/modutram_catenary_pole_era_c.mdl",
			assets = {}
		},
	},
	cost = 40.0,
}
end
