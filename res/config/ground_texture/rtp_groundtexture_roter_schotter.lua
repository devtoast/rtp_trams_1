local tu = require "texutil"

function data()
return {
	texture = tu.makeMaterialIndexTexture("res/textures/terrain/material/country_sidewalk.tga", "REPEAT", "REPEAT"),
	texSize = { 8.0, 0.5 },
	materialIndexMap = {
		[160] = "rtp_terrain_roter_schotter.lua",
		[255] = "rtp_terrain_roter_schotter.lua",
	},
	
	priority = 6
}
end
