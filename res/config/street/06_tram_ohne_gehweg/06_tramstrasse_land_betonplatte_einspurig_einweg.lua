function data()
return {
	laneConfig = {
		{ forward = true },
		{ forward = true },
		{ forward = true },
	},
	streetWidth = 3.0,
	sidewalkWidth = 1.0,
	sidewalkHeight = .0,
	yearFrom = 1850,
	yearTo = 0,
	upgrade = false,
	country = true,
	speed = 70.0,
	transportModesStreet = { "BUS", "TRAM" },
	type = "one way country new small",
	name = _("Tram ohne Gehweg, Betonplatte").." (11)",
	desc = _("Oneway Tram, 1-lane, speed limit %2%, Size Nm(1000mm)"),
	categories = { "06_Tram_ohne_Gehweg" },
	materials = {	
		streetPaving = {
			name = "street/airport/airport_runway_medium_paving.mtl",
			size = { 2.0, 2.0 }
		},	
		streetLane = {
			name = "street/rtp_streetlane_beton.mtl",
			size = { 2.0, 3.0 }
		},
		sidewalkPaving = {
			name = "street/airport/airport_runway_medium_paving.mtl",
			size = { 2.0, 1.0 }	
		},
		sidewalkLane = {
			name = "street/rtp_streetlane_beton.mtl",
			size = { 2.0, 1.0 }	
		},		
		streetTram = {
			name = "street/new_medium_tram_paving.mtl",
			size = { 2.0, 2.0 }
		},
		streetTramTrack = {
			name = "street/new_medium_tram_track.mtl",
			size = { 2.0, 2.0 }	
		},
		crossingTramTrack = {
			name = "street/new_medium_tram_track.mtl",
			size = { 2.0, 2.0 }
		},
	},
	cost = 25.0,
}
end
