local tu = require "texutil"

function data()
return {
	texture = tu.makeMaterialIndexTexture("res/textures/terrain/material/country_sidewalk.tga", "REPEAT", "REPEAT"),
	texSize = { 32.0, 1.5 },
	materialIndexMap = {
		[160] = "shared/grass_cutted_02.lua",
		[255] = "shared/gravel_03.lua",
	},
	
	priority = 6
}
end
