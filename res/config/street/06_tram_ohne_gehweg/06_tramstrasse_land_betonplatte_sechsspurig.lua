function data()
return {
	numLanes = 6,
	streetWidth = 18.0,
	sidewalkWidth = 1.0,
	sidewalkHeight = .0,
	yearFrom = 1850,
	yearTo = 0,
	upgrade = false,
	country = true,
	speed = 70.0,
	transportModesStreet = { "BUS", "TRAM" },
	type = "new medium",
	name = _("Tram ohne Gehweg, Betonplatte").." (26)",
	desc = _("Tram, 6-lane, speed limit %2%, Size Nm(1000mm)"),
	categories = { "06_Tram_ohne_Gehweg" },
	materials = {
		streetPaving = {
			name = "street/airport/airport_runway_medium_paving.mtl",
			size = { 2.0, 2.0 }
		},	
		streetLane = {
			name = "street/rtp_streetlane_beton.mtl",
			size = { 2.0, 3.0 }
		},
		sidewalkPaving = {
			name = "street/airport/airport_runway_medium_paving.mtl",
			size = { 2.0, 1.0 }	
		},
		sidewalkLane = {
			name = "street/rtp_streetlane_beton.mtl",
			size = { 2.0, 1.0 }	
		},		
		streetTram = {
			name = "street/new_medium_tram_paving.mtl",
			size = { 2.0, 2.0 }
		},
		streetTramTrack = {
			name = "street/new_medium_tram_track.mtl",
			size = { 2.0, 2.0 }	
		},
		crossingTramTrack = {
			name = "street/new_medium_tram_track.mtl",
			size = { 2.0, 2.0 }
		},
	},	
	assets = {
		{
			name = "asset/domtrain_kofferleuchte-d.mdl",
			offset = 7.5,
			distance = 10.0,
			prob = 1.0,
			offsetOrth = 10.0,
			randRot = false,
			oneSideOnly = true,
			alignToElevation = false,
			avoidFaceEdges = false,
			placeOnBridge = true,
		}, 
	},
	cost = 40.0,
}
end
