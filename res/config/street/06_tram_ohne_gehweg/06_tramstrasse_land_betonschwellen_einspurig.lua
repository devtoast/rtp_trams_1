function data()
return {
	numLanes = 2,
	streetWidth = 0.05,
	sidewalkWidth = 2.0,
	sidewalkHeight = .0,
	yearFrom = 0,
	yearTo = 9999,
	upgrade = false,
	country = true,
	speed = 70.0,
	transportModesStreet = { "BUS", "TRAM" },
	type = "new medium",
	name = _("Tram ohne Gehweg, Betonschwellen").." (21)",
	desc = _("Tram, 1-lane, speed limit %2%, Size Nm(1000mm)") .. "\n" .. _("Hold 'Shift' while painting with any brush texture to overpaint the ground texture."),
	categories = { "06_Tram_ohne_Gehweg" },
	materials = {
		streetLane = {
			name = "street/rtp_streetlane_betonschwellen.mtl",
			size = { 6.0, 2.2 }
		},	
		streetTramTrack = {
			name = "street/new_medium_tram_track.mtl",
			size = { 2.0, 2.0 }	
		},
		crossingLane = {
			name = "street/rtp_streetlane_betonschwellen.mtl",
			size = { 6, 2.2 }
		},
		crossingTramTrack = {
			name = "street/new_medium_tram_track.mtl",
			size = { 2.0, 2.0 }
		},
	},	
	assets = {
	},
	catenary = {
		pole = {
			name = "asset/modutram_catenary_pole_era_c.mdl",
			assets = {}
		},
	},
	cost = 40.0,
}
end
