function data()
return {
	numLanes = 2,
	streetWidth = 0.05,
	sidewalkWidth = 2.0,
	sidewalkHeight = .0,
	yearFrom = 0,
	yearTo = 9999,
	upgrade = false,
	country = true,
	speed = 70.0,
	transportModesStreet = { "BUS", "TRAM" },
	type = "new medium",
	name = _("Tram ohne Gehweg, Betonplatte").." (21)",
	desc = _("Tram, 1-lane, speed limit %2%, Size Nm(1000mm)"),
	categories = { "06_Tram_ohne_Gehweg" },
	materials = {
		streetPaving = {
			name = "street/airport/airport_runway_medium_paving.mtl",
			size = { 2.0, 3.0 }
		},	
		streetLane = {
			name = "street/rtp_streetlane_beton.mtl",
			size = { 2.0, 3.0 }
		},
		sidewalkPaving = {
			name = "street/airport/airport_runway_medium_paving.mtl",
			size = { 2.0, 2.0 }	
		},
		sidewalkLane = {
			name = "street/rtp_streetlane_beton.mtl",
			size = { 2.0, 2.0 }	
		},
		streetTram = {
			name = "street/new_medium_tram_paving.mtl",
			size = { 2.0, 2.0 }
		},
		streetTramTrack = {
			name = "street/new_medium_tram_track.mtl",
			size = { 2.0, 2.0 }	
		},
		crossingTramTrack = {
			name = "street/new_medium_tram_track.mtl",
			size = { 2.0, 2.0 }
		},
	},	
	assets = {
	},
	signalAssetName = "asset/ampel.mdl",
	cost = 40.0,
}
end
