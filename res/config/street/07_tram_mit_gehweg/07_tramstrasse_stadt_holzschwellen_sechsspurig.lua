function data()
return {
	numLanes = 6,
	streetWidth = 18.0,
	sidewalkWidth = 1.5,
	sidewalkHeight = .3,
	yearFrom = 1850,
	yearTo = 0,
	upgrade = false,
	country = true,
	speed = 70.0,
	transportModesStreet = { "BUS", "TRAM" },
	type = "new medium",
	name = _("Tram mit Gehweg, Holzschwellen").." (26)",
	desc = _("Tram, 6-lane, speed limit %2%, Size Nm(1000mm)") .. "\n" .. _("Hold 'Shift' while painting with any brush texture to overpaint the ground texture."),
	categories = { "07_Tram_mit_Gehweg" },
	materials = {
		streetLane = {
			name = "street/rtp_streetlane_holzschwellen.mtl",
			size = { 6.0, 2.2 }
		},	
		streetTramTrack = {
			name = "street/new_medium_tram_track.mtl",
			size = { 2.0, 2.0 }	
		},
		crossingLane = {
			name = "street/rtp_streetlane_holzschwellen.mtl",
			size = { 6, 2.2 }
		},
		crossingTramTrack = {
			name = "street/new_medium_tram_track.mtl",
			size = { 2.0, 2.0 }
		},
		sidewalkPaving = {
			name = "street/rtp_paving_roter_schotter.mtl",
			size = { 4.0, 1.5 }
		},
		sidewalkBorderInner = {
			name = "street/new_medium_sidewalk_border_inner.mtl",		
			size = { 3, 0.2 }
		},
		sidewalkBorderOuter = {
			name = "street/new_medium_sidewalk_border_outer.mtl",		
			size = { 8.0, 0.1 }
		},
		sidewalkCurb = {
			name = "street/new_medium_sidewalk_curb.mtl",
			size = { 3, .35 }
		},
		sidewalkWall = {
			name = "street/new_medium_sidewalk_wall.mtl",
			size = { 8.0, 0.41602 }
		}
	},	
	assets = {
		{
			name = "asset/domtrain_altstadtleuchte.mdl",
			offset = 7.5,
			distance = 10.0,
			prob = 1.0,
			offsetOrth = 0.2,
			randRot = false,
			oneSideOnly = false,
			alignToElevation = false,
			avoidFaceEdges = false,
			placeOnBridge = true,
		}, 
	},
	catenary = {
		pole = {
			name = "asset/modutram_catenary_pole_era_c.mdl",
			assets = {}
		},
	},
	cost = 40.0,
}
end
