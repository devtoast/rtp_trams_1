function data()
	return {
		en = {
			["Name"] = ("RTP - Tramway Pack"),
			["Description"] = ("Roads´n Trams Project (RTP):\n\nA variety of tramways."),
		
		-- categories --
			["(RTP) Tram ohne Gehweg"] = ("(RTP) tram without sidewalk"),
			["(RTP) Tram mit Gehweg"] = ("(RTP) Tram with sidewalk"),
			["(RTP) Tram Anschluss"] = ("(RTP) tram connector"),
		
		-- Component Name			
			["Tram ohne Gehweg, unsichtbar"] = ("Tram without sidewalk, transparent sleeper"),
			["Tram ohne Gehweg, Holzschwellen"] = ("Tram without sidewalk, wooden sleeper"),
			["Tram ohne Gehweg, Betonschwellen"] = ("Tram without sidewalk, concrete sleeper"),
			["Tram ohne Gehweg, Betonplatte"] = ("Tram without sidewalk, concrete slab"),
			
			["Tram mit Gehweg, unsichtbar"] = ("Tram with sidewalk, transparent sleeper"),
			["Tram mit Gehweg, Holzschwellen"] = ("Tram with sidewalk, wooden sleeper"),
			["Tram mit Gehweg, Betonschwellen"] = ("Tram with sidewalk, concrete sleeper"),
			["Tram mit Gehweg, Betonplatte"] = ("Tram with sidewalk, concrete slab"),
			
			["Tram Anschluss, Holzschwellen"] = ("Tram connector, wooden sleeper"),
			["Tram Anschluss, Betonschwellen"] = ("Tram connector, concrete sleeper"),
		},
        de = {
			["Name"] = ("RTP - Tramgleis Paket"),
			["Description"] = ("Roads´n Trams Project (RTP):\n\nEine vielzahl an Tramgleise für den Schönbau."),
		
		-- categories --
			["(RTP) Tram ohne Gehweg"] = ("(RTP)Tram ohne Gehweg"),
			["(RTP) Tram mit Gehweg"] = ("(RTP) Tram mit Gehweg"),
			["(RTP) Tram Anschluss"] = ("(RTP) Tram Anschluss"),		
		
		-- Component Name			
			["Tram ohne Gehweg, unsichtbar"] = ("Tram ohne Gehweg, unsichtbar"),
			["Tram ohne Gehweg, Holzschwellen"] = ("Tram ohne Gehweg, Holzschwellen"),
			["Tram ohne Gehweg, Betonschwellen"] = ("Tram ohne Gehweg, Betonschwellen"),
			["Tram ohne Gehweg, Betonplatte"] = ("Tram ohne Gehweg, Betonplatte"),
			
			["Tram mit Gehweg, unsichtbar"] = ("Tram mit Gehweg, unsichtbar"),
			["Tram mit Gehweg, Holzschwellen"] = ("Tram mit Gehweg, Holzschwellen"),
			["Tram mit Gehweg, Betonschwellen"] = ("Tram mit Gehweg, Betonschwellen"),
			["Tram mit Gehweg, Betonplatte"] = ("Tram mit Gehweg, Betonplatte"),
			
			["Tram Anschluss, Holzschwellen"] = ("Tram Anschluss, Holzschwellen"),
			["Tram Anschluss, Betonschwellen"] = ("Tram Anschluss, Betonschwellen"),

		-- Component Descritption
			["Hold 'Shift' while painting with any brush texture to overpaint the ground texture."] = ("Halte 'Shift'-Taste gedrückt um mit dem Mal-Tool darüber zu malen."),

			["Oneway Tram, 1-lane, speed limit %2%, Size Nm(1000mm)"] = ("Einbahn-Tram 1-spurig, Tempo %2%, Spur Nm(1000mm)"),
			["Oneway Tram, 2-lane, speed limit %2%, Size Nm(1000mm)"] = ("Einbahn-Tram 2-spurig, Tempo %2%, Spur Nm(1000mm)"),			
			["Oneway Tram, 3-lane, speed limit %2%, Size Nm(1000mm)"] = ("Einbahn-Tram 3-spurig, Tempo %2%, Spur Nm(1000mm)"),
			
			["Tram, 1-lane, speed limit %2%, Size Nm(1000mm)"] = ("Tram, 1-spurig, Tempo %2%, Spur Nm(1000mm)"),
			["Tram, 2-lane, speed limit %2%, Size Nm(1000mm)"] = ("Tram, 2-spurig, Tempo %2%, Spur Nm(1000mm)"),
			["Tram, 4-lane, speed limit %2%, Size Nm(1000mm)"] = ("Tram, 4-spurig, Tempo %2%, Spur Nm(1000mm)"),
			["Tram, 6-lane, speed limit %2%, Size Nm(1000mm)"] = ("Tram, 6-spurig, Tempo %2%, Spur Nm(1000mm)"),
		},
	}
end
