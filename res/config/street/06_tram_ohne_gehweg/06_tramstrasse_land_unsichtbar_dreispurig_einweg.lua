function data()
return {
	laneConfig = {
		{ forward = true },
		{ forward = true },
		{ forward = true },
		{ forward = true },
		{ forward = true },
	},
	streetWidth = 9.0,
	sidewalkWidth = 0.7,
	sidewalkHeight = .0,
	yearFrom = 0,
	yearTo = 9999,
	upgrade = false,
	country = true,
	speed = 70.0,
	transportModesStreet = { "BUS", "TRAM" },
	type = "one way country new small",
	name = _("Tram ohne Gehweg, unsichtbar").." (13)",
	desc = _("Oneway Tram, 3-lane, speed limit %2%, Size Nm(1000mm)") .. "\n" .. _("Hold 'Shift' while painting with any brush texture to overpaint the ground texture."),
	categories = { "06_Tram_ohne_Gehweg" },
	materials = {	
		streetTramTrack = {
			name = "street/new_medium_tram_track.mtl",
			size = { 2.0, 2.0 }	
		},
		crossingTramTrack = {
			name = "street/new_medium_tram_track.mtl",
			size = { 2.0, 2.0 }
		},
	},
	catenary = {
		pole = {
			name = "asset/modutram_catenary_pole_era_c.mdl",
			assets = {}
		},
	},
	cost = 25.0,
}
end
