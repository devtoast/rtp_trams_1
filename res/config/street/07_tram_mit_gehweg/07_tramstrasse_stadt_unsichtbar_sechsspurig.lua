function data()
return {
	numLanes = 6,
	streetWidth = 18.0,
	sidewalkWidth = 1.5,
	sidewalkHeight = .0,
	yearFrom = 1850,
	yearTo = 0,
	upgrade = false,
	country = true,
	speed = 70.0,
	transportModesStreet = { "BUS", "TRAM" },
	type = "new medium",
	name = _("Tram mit Gehweg, unsichtbar").." (26)",
	desc = _("Tram, 6-lane, speed limit %2%, Size Nm(1000mm)") .. "\n" .. _("Hold 'Shift' while painting with any brush texture to overpaint the ground texture."),
	categories = { "07_Tram_mit_Gehweg" },
	materials = {	
		streetTramTrack = {
			name = "street/new_medium_tram_track.mtl",
			size = { 2.0, 2.0 }	
		},
		crossingTramTrack = {
			name = "street/new_medium_tram_track.mtl",
			size = { 2.0, 2.0 }
		},
	},	
	assets = {
		{
			name = "asset/domtrain_kofferleuchte-d.mdl",
			offset = 7.5,
			distance = 10.0,
			prob = 1.0,
			offsetOrth = 10.5,
			randRot = false,
			oneSideOnly = true,
			alignToElevation = false,
			avoidFaceEdges = false,
			placeOnBridge = true,
		}, 
	},
	catenary = {
		pole = {
			name = "asset/modutram_catenary_pole_era_c.mdl",
			assets = {}
		},
	},
	sidewalkFillGroundTex = "rtp_groundtexture_sidewalk_grasscutted02_gravel03.lua",
	cost = 40.0,
}
end
