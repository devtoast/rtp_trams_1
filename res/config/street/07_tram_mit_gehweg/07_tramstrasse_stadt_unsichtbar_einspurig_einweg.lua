function data()
return {
	laneConfig = {
		{ forward = true },
		{ forward = true },
		{ forward = true },
	},
	streetWidth = 3.0,
	sidewalkWidth = 1.5,
	sidewalkHeight = .0,
	yearFrom = 1850,
	yearTo = 0,
	upgrade = false,
	country = true,
	speed = 70.0,
	transportModesStreet = { "BUS", "TRAM" },
	type = "one way country new small",
	name = _("Tram mit Gehweg, unsichtbar").." (11)",
	desc = _("Oneway Tram, 1-lane, speed limit %2%, Size Nm(1000mm)") .. "\n" .. _("Hold 'Shift' while painting with any brush texture to overpaint the ground texture."),
	categories = { "07_Tram_mit_Gehweg" },
	materials = {	
		streetTramTrack = {
			name = "street/new_medium_tram_track.mtl",
			size = { 2.0, 2.0 }	
		},
		crossingTramTrack = {
			name = "street/new_medium_tram_track.mtl",
			size = { 2.0, 2.0 }
		},
	},
	catenary = {
		pole = {
			name = "asset/modutram_catenary_pole_era_c.mdl",
			assets = {}
		},
	},
	sidewalkFillGroundTex = "rtp_groundtexture_sidewalk_grasscutted02_gravel03.lua",
	cost = 25.0,
}
end
