function data()
return {
  info = {
    minorVersion = 0,
    severityAdd = "NONE",
    severityRemove = "WARNING",
    name = _("Name"),
    description = _("Description"),
	tags = { "Tram","Street",},
	visible = true,
    authors = {
      {
		name = "easybronko",
		role = "Models & Script",
      },
      {
		name = "wicked1133",
		role = "UI & Script",
      },
      {
		name = "Yoshi",
		role = "Textures,Assets",
      },
      {
		name = "DomTrain",
		role = "Streetlights",
      },
    },
  },
}
end