function data()
return {
	numLanes = 4,
	streetWidth = 12.0,
	sidewalkWidth = 1.5,
	sidewalkHeight = .3,
	yearFrom = 1850,
	yearTo = 0,
	upgrade = false,
	country = true,
	speed = 70.0,
	transportModesStreet = { "BUS", "TRAM" },
	type = "new medium",
	name = _("Tram mit Gehweg, Betonplatte").." (24)",
	desc = _("Tram, 4-lane, speed limit %2%, Size Nm(1000mm)"),
	categories = { "07_Tram_mit_Gehweg" },
	materials = {
		streetPaving = {
			name = "street/airport/airport_runway_medium_paving.mtl",
			size = { 2.0, 3.0 }
		},	
		streetLane = {
			name = "street/rtp_streetlane_beton.mtl",
			size = { 2.0, 3.0 }
		},		
		streetTram = {
			name = "street/new_medium_tram_paving.mtl",
			size = { 2.0, 2.0 }
		},
		streetTramTrack = {
			name = "street/new_medium_tram_track.mtl",
			size = { 2.0, 2.0 }	
		},
		crossingTramTrack = {
			name = "street/new_medium_tram_track.mtl",
			size = { 2.0, 2.0 }
		},
		sidewalkPaving = {
			name = "street/airport/airport_runway_medium_paving.mtl",
			size = { 4.0, 1.5 }
		},
		sidewalkBorderOuter = {
			name = "street/new_medium_sidewalk_border_outer.mtl",		
			size = { 8.0, 0.41602 }
		},
		sidewalkCurb = {
			name = "street/new_medium_sidewalk_curb.mtl",
			size = { 3, .35 }
		},
		sidewalkWall = {
			name = "street/new_medium_sidewalk_wall.mtl",
			size = { 8.0, 0.41602 }
		}	
	},	
	assets = {
		{
			name = "asset/domtrain_kofferleuchte-d.mdl",
			offset = 7.5,
			distance = 10.0,
			prob = 1.0,
			offsetOrth = 7.5,
			randRot = false,
			oneSideOnly = true,
			alignToElevation = false,
			avoidFaceEdges = false,
			placeOnBridge = true,
		}, 
	},
	cost = 40.0,
}
end
